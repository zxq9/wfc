%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% a word is the smallest unit in a reduced sum. In for instance
%
% 1 + A + AB, the words are 1, A, and AB, which are represented as
% the sets {}, {a}, and {a, b}, respectively.
%
% a word is a tagged-tuple {w, <set of atoms>}; the empty set equals 1
%
% in WF algebra, anything times itself equals itself, therefore we
% don't need to keep track of exponents. That is why the set
% representation makes sense.
%
% with a word, multiplication is implied
% with a sentence, summation is implied
-module(wfc_word).

-compile(export_all).
-export_type([word/0]).

-type wfchar() :: atom().
-type word() :: {w, sets:set(wfchar())}.
-type map01() :: #{atom() := 0 | 1}.

-spec one() -> word().
-spec is_one(term()) -> boolean().
-spec is_valid_word(term()) -> boolean().
-spec all_atoms(word()) -> boolean().
-spec from_list([wfchar()]) -> word().
-spec to_list(word()) -> [wfchar()].
-spec times(word(), word()) -> word().
-spec eval(word(), map01()) -> 0 | 1.
-spec really_eval(word(), map01()) -> 0 | 1.
-spec pf(word()) -> string().

% a set is one if it is empty
one() ->
    {w, sets:new()}.

% a set is one if it is empty
is_one(Word) ->
    Word =:= one().


% is valid if exactly one of these conditions are true:
%
%   - is empty
%   - contains only atoms
is_valid_word(Word) ->
    is_one(Word) orelse all_atoms(Word).

% returns true if all elements in the set are atoms
all_atoms({w, Set}) ->
    Pred =
        fun(Element, Accumulator) ->
            Accumulator andalso is_atom(Element)
        end,
    sets:fold(Pred, true, Set);
all_atoms(_) ->
    false.


from_list(List) ->
    Set = sets:from_list(List),
    Word = {w, Set},
    true = is_valid_word(Word),
    Word.


to_list({w, Set}) ->
    sets:to_list(Set).


% multiplication of two words is just the union; minus removing one
times(WL, WR) ->
    LOne = is_one(WL),
    ROne = is_one(WR),
    Word =
        if
            % multiplying by 1 just gives the other
            LOne ->
                WR;
            ROne ->
                WL;
            % otherwise take the unions of the things it contains
            true ->
                {w, L} = WL,
                {w, R} = WR,
                LR = sets:union(L, R),
                {w, LR}
        end,
    true = is_valid_word(Word),
    Word.


% evaluate a word; given a word, and a map that maps every atom to 1
% or 0, take the product of all the atoms in the word
eval(Word, Map) ->
    IsOne = is_one(Word),
    if
        IsOne ->
            1;
        true ->
            really_eval(Word, Map)
    end.

really_eval({w, Set}, Map) ->
    Folder =
        fun(Atom, Accum) ->
            Val01 = maps:get(Atom, Map),
            NewAccum = Val01 * Accum,
            NewAccum
        end,
    sets:fold(Folder, 1, Set).


% returns string
%
% "(lprod           if word is 1
% "(lprod a b c)"   if word is the set containing {a,b,c}
pf(Word) ->
    true = is_valid_word(Word),
    IsOne = is_one(Word),
    Strs =
        if
            IsOne ->
                "";
            true ->
                Atoms = to_list(Word),
                pf_atoms(Atoms, [])
        end,
    ["(lprod", Strs, ")"].


pf_atoms([], Accum) ->
    Accum;
pf_atoms([A | As], Accum) ->
    S = atom_to_list(A),
    NewAccum = [Accum, " ", S],
    pf_atoms(As, NewAccum).
