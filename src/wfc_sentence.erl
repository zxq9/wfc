%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% sentences are a set of words
%
% with a word, multiplication is implied
% with a sentence, summation is implied
-module(wfc_sentence).

-compile(export_all).
-export_type([sentence/0]).

-type word() :: wfc_word:word().
-type sentence() :: {s, sets:set(word())}.
-type set() :: sets:set().
-type map01() :: #{atom() := 0 | 1}.

-spec one() -> sentence().
-spec zero() -> sentence().
-spec is_one(term()) -> boolean().
-spec is_zero(term()) -> boolean().
-spec is_valid_sentence(term()) -> boolean().
-spec all_words_none_zero(sentence()) -> boolean().
-spec set_symdiff(set(), set()) -> set().
-spec from_list([word()]) -> sentence().
-spec to_list(sentence()) -> [word()].
-spec plus(sentence(), sentence()) -> sentence().
-spec sum([sentence()]) -> sentence().
-spec times(sentence(), sentence()) -> sentence().
-spec prod([sentence()]) -> sentence().
-spec sxs(sentence(), sentence()) -> sentence().
-spec wxs(word(), sentence()) -> sentence().
-spec wxw(word(), word()) -> sentence().
-spec from_wfchar(WfChar) -> sentence()
            when WfChar :: 0 | 1 | atom().
-spec eval(sentence(), map01()) -> 0 | 1.
-spec really_eval(sentence(), map01()) -> 0 | 1.
-spec pf(sentence()) -> string().

% the one-sentence is the sentence containing just the one-word
one() ->
    WordOne = wfc_word:one(),
    {s, sets:from_list([WordOne])}.

is_one(X) ->
    X =:= one().

% the zero-sentence is the empty sentence
zero() ->
    {s, sets:new()}.

% a sentence is zero if it is empty
is_zero(Sent) ->
    Sent =:= zero().


% is valid if exactly one of these conditions are true:
%
%   - is empty
%   - contains only words, none of which are the zero word
is_valid_sentence(Sent) ->
    is_zero(Sent) orelse all_words_none_zero(Sent).

all_words_none_zero({s, Set}) ->
    Pred =
        fun(Elem, Accum) ->
            Accum
                andalso wfc_word:is_valid_word(Elem)
        end,
    sets:fold(Pred, true, Set);
all_words_none_zero(_) ->
    false.

% symmetric difference, this is the way we add sentences:
set_symdiff(X, Y) ->
    Cup = sets:union(X, Y),
    Cap = sets:intersection(X, Y),
    sets:subtract(Cup, Cap).


% from a list of WORDS, not a list of atoms
from_list(List) ->
    Set = sets:from_list(List),
    Sent = {s, Set},
    true = is_valid_sentence(Sent),
    Sent.

% to a list of WORDS, not a list of atoms
to_list({s, Set}) ->
    sets:to_list(Set).


% addition of two sentences is just the symmetric difference
plus({s, L}, {s, R}) ->
    SetLR = set_symdiff(L, R),
    Sent = {s, SetLR},
    true = is_valid_sentence(Sent),
    Sent.

% sum of a list of SENTENCES
sum(List) ->
    Foldel =
        fun(Sent, Accum) ->
            % this does validation at every step
            plus(Sent, Accum)
        end,
    Result = lists:foldl(Foldel, zero(), List),
    % but more validation isn't bad
    true = is_valid_sentence(Result),
    Result.


% alias for sxs
times(SL, SR) ->
    sxs(SL, SR).


% product of a list of SENTENCES
prod(List) ->
    Foldel =
        fun(Sent, Accum) ->
            % this does validation at every step
            times(Sent, Accum)
        end,
    Result = lists:foldl(Foldel, one(), List),
    % but more validation isn't bad
    true = is_valid_sentence(Result),
    Result.

% sentence times sentence
sxs(SentL, SentR) ->
    true = is_valid_sentence(SentL),
    true = is_valid_sentence(SentR),
    ZeroL = is_zero(SentL),
    ZeroR = is_zero(SentR),
    OneL = is_one(SentL),
    OneR = is_one(SentR),
    Result =
        if
            ZeroL orelse ZeroR ->
                zero();
            OneL ->
                SentR;
            OneR ->
                SentL;
            true ->
                really_sxs(SentL, SentR)
        end,
    true = is_valid_sentence(Result),
    Result.

really_sxs(SentL, SentR) ->
    % fold over list of words in the left sentence, multiply through
    % by right sentence, take sum of result
    Fold =
        fun(WordL, AccumSentence) ->
            Summand = wxs(WordL, SentR),
            NewAccum = plus(Summand, AccumSentence),
            true = is_valid_sentence(NewAccum),
            NewAccum
        end,
    {s, SetL} = SentL,
    Result = sets:fold(Fold, zero(), SetL),
    true = is_valid_sentence(Result),
    Result.


wxs(WordL, SentR) ->
    Fold =
        fun(WordR, AccumSentence) ->
            Summand = wxw(WordL, WordR),
            NewAccum = plus(Summand, AccumSentence),
            true = is_valid_sentence(NewAccum),
            NewAccum
        end,
    {s, SetR} = SentR,
    Result = sets:fold(Fold, zero(), SetR),
    true = is_valid_sentence(Result),
    Result.


wxw(WordL, WordR) ->
    NewWord = wfc_word:times(WordL, WordR),
    true = wfc_word:is_valid_word(NewWord),
    Sent = from_list([NewWord]),
    true = is_valid_sentence(Sent),
    Sent.


from_wfchar(0) ->
    zero();
from_wfchar(1) ->
    one();
from_wfchar(Atom) when is_atom(Atom) ->
    Word = wfc_word:from_list([Atom]),
    true = wfc_word:is_valid_word(Word),
    Sent = from_list([Word]),
    true = is_valid_sentence(Sent),
    Sent.


from_word(Word) ->
    true = wfc_word:is_valid_word(Word),
    Sent = from_list([Word]),
    true = is_valid_sentence(Sent),
    Sent.


eval(Sent, Map) ->
    IsZero = is_zero(Sent),
    IsOne = is_one(Sent),
    if
        IsZero ->
            0;
        IsOne ->
            1;
        true ->
            really_eval(Sent, Map)
    end.

really_eval({s, WordSet}, Map) ->
    Fold =
        fun(Word, Accum) ->
            Val01 = wfc_word:eval(Word, Map),
            NewAccum = lxor(Val01, Accum),
            NewAccum
        end,
    sets:fold(Fold, 0, WordSet).

lxor(0, 0) -> 0;
lxor(0, 1) -> 1;
lxor(1, 0) -> 1;
lxor(1, 1) -> 0.

% returns string
%
% "(lsum                                       if sentence is 0
% "(lsum (lprod 1) (lprod a) (lprod a b))"     if sentence is 1 + a + ab
pf(Sent) ->
    true = is_valid_sentence(Sent),
    IsZero = is_zero(Sent),
    Strs =
        if
            % zero word should never be constructed but ehhh who cares
            IsZero ->
                "";
            true ->
                Words = to_list(Sent),
                pf_words(Words, [])
        end,
    ["(lsum", Strs, ")"].


pf_words([], Accum) ->
    Accum;
pf_words([W | Ws], Accum) ->
    S = wfc_word:pf(W),
    NewAccum = [Accum, " ", S],
    pf_words(Ws, NewAccum).
