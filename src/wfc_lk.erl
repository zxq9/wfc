%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% lying knights problem
-module(wfc_lk).

-compile(export_all).

% knight of the golden road is a liar
% "this road leads to the grail. also if the stones take you there,
% so does the marble road"
gold_says() ->
    wand(gold,
         wimplies(stones, marble)).

% marble guy is a liar too
% "neither the gold or stone lead to the grail"
marble_says() ->
    wand(wnot(gold), wnot(stone)).


% stone guy is also an asshole
% "follow the gold and you'll reach the grail, follow the marble and
% you'll be lost"
stone_says() ->
    wand(gold, wnot(marble)).


all_liars() ->
    % small jogritude: and is binary, but this is fine
    wand(
        wnot(gold_says()),
        wand(wnot(marble_says()),
             wnot(stone_says()))).

is_gold() ->
    pf(wimplies(all_liars(), gold)).

is_marble() ->
    pf(wimplies(all_liars(), marble)).

is_stone() ->
    pf(wimplies(all_liars(), stone)).

% connectives just copied
pf(X) ->
    wfc:pf(X).

wxor(A, B) ->
    wfc:wxor(A, B).

wand(A, B) ->
    wfc:wand(A, B).

wnot(A) ->
    wfc:wnot(A).

wior(A, B) ->
    wfc:wior(A, B).

wimplies(A, B) ->
    wfc:wimplies(A, B).

wimpliedby(A, B) ->
    wfc:wimpliedby(A, B).

wiff(A, B) ->
    wfc:wiff(A, B).

